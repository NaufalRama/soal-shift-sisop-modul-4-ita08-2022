# soal-shift-sisop-modul-4-ITA08-2022



Anggota Kelompok :
1. Ilham Muhammad Sakti - 5027201042
2. Gennaro Fajar Mennde - 5027201061
3. Naufal Ramadhan - 5027201067

## Daftar Isi
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)

# Soal:
https://docs.google.com/document/d/13HAYs1Dp9-gZ0RtFUQqf-SULoj4dL9JG0LoiKgrOY7Y/edit

# Soal 1 

## Soal 1

### Soal 1 - A
Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13	
Contoh : 
“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”
```
void enkripsi1(char *string)
{
  if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0)
    return;

  char a[10];
  char new[1000];
  int totalN = 0, flag = 0, ext = 0;
  memset(a, 0, sizeof(a));
  memset(new, 0, sizeof(new));

  for (int i = 0; i < strlen(string); i++)
  {
    if (string[i] == '/')
      continue;
    if (string[i] == '.')
    {
      new[totalN++] = string[i];
      flag = 1;
    }
    if (flag == 1)
      a[ext++] = string[i];
    else
      new[totalN++] = string[i];
  }

  for (int i = 0; i < totalN; i++)
  {
    if (isupper(new[i]))
      new[i] = 'A' + 'Z' - new[i];
    else if (islower(new[i]))
    {
      if (new[i] > 109)
        new[i] -= 13;
      else
        new[i] += 13;
    }
  }

  strcat(new, a);
  strcpy(string, new);
}
```

Berdasarkan kode di atas, encode dilakukan pada fungsi `enkripsi1`. Pertama-tama di cek apakah ada file "." dan "..", jika bernilai benar akan dihentikan fungsinya. Setelah itu, dibuat variabel `a` untuk menampung extension suatu file dan `new` untuk menampung nama file atau folder yang akan diencode(tanpa extension) yang masing-masing variabel bertipe string. Masing-masing variabel akan dilakukan `memset` yang bertujuan mengisi setiap char pada string supaya tidak menghasilkan karakter aneh pada output. Variabel lain yang dibuat yaitu `ext` yang akan memiliki panjang dari extension, `totalN` yang akan memiliki panjang dari nama file, dan `flag` yang digunakan sebagai penanda antara nama file dan extensionnya yang semua variabel tersebut diset memiliki nilai 0. Selanjutnya, akan dicari nama file atau folder beserta extensionnya jika ada yaitu dengan melakukan perulangan sebanyak `strlen(string)` atau panjang dari file. Pada perulangan, jika `string[i]` merupakan `.`, maka pada `new` akan ditambahkan `.` dan `flag` akan berubah nilainya menjadi 1 yang berarti sudah masuk pada fase extension. Namun, jika `string[i]` bukan merupakan `.`, maka akan diperiksa terlebih dahulu `flag` bernilai 0 atau 1, jika bernilai 0, maka `string[i]` akan ditambahkan pada `new`, jika sebaliknya, maka `string[i]` akan ditambahkan pada `a`. `totalN` akan bertambah ketika `new` bertambah dan `ext` akan bertambah ketika `a` bertambah. Setelah itu, `new` akan diencode dengan perulangan sebanyak `totalN`. Pada encode yang dilakukan, akan diperiksa apakah `new[i]` adalah **uppercase** atau **lowercase**. Jika merupakan **uppercase**, maka yang akan dilakukan adalah encode dengan atbash cipher yaitu menggunakan formula `'A' +'z' - new[i]`, tetapi jika merupakan **lowercase**, maka yang akan dilakukan adalah encode dengan rot13 yaitu penambahan 13 atau pengurangan 13. Pada encode dengan rot13, jika `newFile` merupakan karakter `a` hingga `m` maka akan dilakukan penambahan 13, tetapi jika `new` merupakan karakter `n` hingga `z` maka akan dilakukan pengurangan 13.
<div align="center">
    ![encode](https://i.ibb.co/rvJfPcD/encode.png)
</div>
Lalu, `new` dan `a` akan digabungkan menggunakan `strcat()` dan hasilnya akan dicopy pada `string` menggunakan `strcpy()`.

### Soal 1 - B dan C
  Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a. Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

```
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};
```

```
static int xmp_getattr(const char *path, struct stat *stbuf)
{
  char *str = strstr(path, prefix);
  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }

  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = lstat(fpath, stbuf);
  if (res == -1)
    return -errno;

  return 0;
}
```

```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
  char *str = strstr(path, prefix);
  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }

  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  DIR *dp;
  struct dirent *de;
  (void)offset;
  (void)fi;
  dp = opendir(fpath);

  if (dp == NULL)
    return -errno;

  while ((de = readdir(dp)) != NULL)
  {
    if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
      continue;
    struct stat st;
    memset(&st, 0, sizeof(st));
    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;

    if (str != NULL)
      enkripsi1(de->d_name);
    if (str2 != NULL)
      enskripsi2(de->d_name);
    if (str3 != NULL)
      getSpecialFile(de->d_name);

    res = (filler(buf, de->d_name, &st, 0));
    if (res != 0)
      break;
  }

  logged2("READDIR", fpath, "");

  closedir(dp);
  return 0;
}
```

```
static int xmp_rename(const char *source, const char *destination)
{
  int res;
  char fpath_source[1000];
  char fpath_destination[1000];
  char *p_fpath_source, *p_fpath_destination;

  if (strcmp(source, "/") == 0)
  {
    source = dirpath;
    sprintf(fpath_source, "%s", source);
  }
  else
    sprintf(fpath_source, "%s%s", dirpath, source);

  if (strcmp(source, "/") == 0)
    sprintf(fpath_destination, "%s", dirpath);
  else
    sprintf(fpath_destination, "%s%s", dirpath, destination);

  res = rename(fpath_source, fpath_destination);
  if (res == -1)
    return -errno;

  p_fpath_source = strrchr(fpath_source, '/');
  p_fpath_destination = strrchr(fpath_destination, '/');
  if (strstr(p_fpath_source, "Animeku_"))
    logged("RENAME", "terdecode", fpath_source, fpath_destination);
  if (strstr(p_fpath_destination, "Animeku_"))
    logged("RENAME", "terenkripsi", fpath_source, fpath_destination);

  logged2("RENAME", fpath_source, fpath_destination);

  return 0;
}
```

Untuk Source code diatas untuk dapat menyelesaikan permasalahan pada no 1 dibutuhkan operasi getattr, readdir, read, dan operasi rename. Dari source code diatas yang pertama kita lakukan adalah mencari tau apakah nama file tersebut mengandung prefix yang diminta dalam hal soal no 1, prefix yang diminta adalah "Animeku_", maka dapat digunakan method stl pada string dengan `strstr(path, prefix)` dimana variabel `prefix` merupakan array yang berisi "Animeku_". Lalu dicek apakah variabel `str` berisi atau tidak, jika tidak berisi maka artinya tidak ditemukan file dengan awalan "Animeku_" sedangkan jika bertemu maka diartikan bahwa ditemukan file yang berawalan "Animeku_". Lalu kita mau memindahkan kursor address dari index ke - 0 menjadi nama file atau folder pertama setelah prefix folder, maka kita akan menambahkan kursor tersebut sebanyak panjang prefix yang diminta lalu tambahkan method stl `strchr(str,'/')` yang artinya itu menemukan folder setelah folder dengan awalan prefix. Lalu sisa dari string tersebut akan di dienkripsikan atau didecode menggunakan function `enkripsi1(temp)` hal ini dilakukan karena decode dan encode untuk no 1 sama persis.


### Soal 1 - D
  Setiap data yang terencode akan masuk dalam file **“Wibu.log”**. Contoh isi: 
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba

```
void logged(char *command, char *msg, char *old, char *new)
{
  FILE *fptr;
  fptr = fopen("Wibu.log", "a");
  if (fptr == NULL)
  {
    printf("[Error] : [Gagal dalam membuka file]");
    exit(1);
  }
  fprintf(fptr, "%s\t%s\t%s\t-->\t%s\n", command, msg, old, new);
  fclose(fptr);
}
```
Pertama-tama kita lakukan adalah membuka file log tersebut yaitu "Wibu.log" dengan mode "a" yaitu *append*. Lalu dicek apakah file berhasil dibuka atau tidak, jika tidak maka outputkan errorrnya dan melakukan `exit(1)`. Jika berhasil maka printf kan ke dalam file tersebut sesuai dengan format yang ada dalam soal ke-1. Tutup file jika sudah tidak digunakkan lagi. Pencatatan log akan dilakukan pada rename yaitu di method `xmp_rename` dengan melakukan pengecekan pada variabel `p_fpath_source` atau `p_fpath_destination` apakah mengandung kata prefix yang diminta, jika `p_fpath_source` mengandung kata prefix yang diminta artinya file tersebut akan di decode sedangkan jika `p_fpath_destination` yang tidak kosong alias terdapat prefix yang diminta artinya file tersebut akan di enkripsikan.

### Hasil
![hasil1](image/sisop.JPG)

### Kendala
- Sulit untuk memahami materi karena contoh program sedikit.
- Kesulitan dalam manipulasi string.

## Soal 2

### Soal 2 - A
Jika suatu direktori dibuat dengan awalan **“IAN_[nama]”**, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma **Vigenere Cipher** dengan key **“INNUGANTENG”** (Case-sensitive, Vigenere).

```
void enskripsi2(char *string)
{
  if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0)
    return;
  int mark = 0;
  for (int i = 0; i < strlen(string); i++)
  {
    if (key[mark] == '\0')
      mark = 0;
    if (isupper(string[i]))
      string[i] = (string[i] - 'A' + key[mark++] - 'A') % 26 + 'A';
    else if (islower(string[i]))
      string[i] = (string[i] - 'a' + tolower(key[mark++]) - 'a') % 26 + 'a';
    else
      mark = 0;
  }
}
```

```
void decode2(char *string)
{
  if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0)
    return;
  int mark = 0;
  for (int i = 0; i < strlen(string); i++)
  {
    if (key[mark] == '\0')
      mark = 0;
    if (isupper(string[i]))
      string[i] = (string[i] - 'A' - key[mark++] + 'A' + 26) % 26 + 'A';
    else if (islower(string[i]))
      string[i] = (string[i] - 'a' - tolower(key[mark++]) + 'a' + 26) % 26 + 'a';
    else
      mark = 0;
  }
}
```
Encode dilakukan dengan pembuatan function `enskripsi2(char *string)`. Function `enskripsi2(char *string)` akan memiliki argumen `char *string` yang merupakan nama file atau folder yang akan diencode. Sedangkan function `void decode2(char *string)` akan memiliki argumen `char *string` yang merupakan nama file atau folder yang akan didecode. Pada function `enskripsi2(char *string)` akan diperiksa terlebih dahulu apakah `file` berisi `.` atau `..`, jika merupakan salah satu dari pilihan tersebut, maka encode tidak perlu dilakukan. Setelah itu, `string` akan diencode dengan perulangan sebanyak `strlen(string)`. Pada encode yang dilakukan, akan diperiksa apakah `string[i]` adalah **uppercase** atau **lowercase** serta apakah `key[mark] == '\0`. Jika merupakan **uppercase**, maka yang akan dilakukan adalah encode dengan Vigenere Cipher yaitu menggunakan formula `string[i] = (string[i] - 'A' + key[mark++] - 'A') % 26 + 'A'`, tetapi jika merupakan **lowercase**, maka \formula yang akan digunakan adalah `string[i] = (string[i] - 'a' + tolower(key[mark++]) - 'a') % 26 + 'a'`. Jika `string` akan didecode, akan dilakukan hal yang sama sesuai penjelasana no 2 hanya saja ada perbedaan formula, yaitu jika `string[i]` merupakan **uppercase** akan digunakkan formula `string[i] = (string[i] - 'A' - key[mark++] + 'A' + 26) % 26 + 'A'` sedangkan jika `string[i]` merupakan **lowercase** akan digunakkan formula `string[i] = (string[i] - 'a' - tolower(key[mark++]) + 'a' + 26) % 26 + 'a'`

### Soal 2 - B + C
  Jika suatu direktori di rename dengan **“IAN_[nama]”**, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a. Apabila nama direktori dihilangkan **“IAN_”**, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.

```
static int xmp_getattr(const char *path, struct stat *stbuf)
{
  char *str = strstr(path, prefix);
  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }

  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = lstat(fpath, stbuf);
  if (res == -1)
    return -errno;

  return 0;
}
```

```
  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }
```
Pertama-tama kita lakukan adalah mencari tau apakah nama file tersebut mengandung prefix yang diminta dalam hal soal no 2, prefix yang diminta adalah "IAN_", maka dapat digunakan method stl pada string dengan `strstr(path, prefix2)` dimana variabel `prefix2` merupakan array yang berisi "IAN_". Lalu dicek apakah variabel `str2` berisi atau tidak, jika tidak berisi maka artinya tidak ditemukan file dengan awalan "IAN_" sedangkan jika bertemu maka diartikan bahwa ditemukan file yang berawalan "IAN_". Lalu kita mau memindahkan kursor address dari index ke - 0 menjadi nama file atau folder pertama setelah prefix folder, maka kita akan menambahkan kursor tersebut sebanyak panjang prefix yang diminta lalu tambahkan method stl `strchr(str2,'/')` yang artinya itu menemukan folder setelah folder dengan awalan prefix. Lalu sisa dari string tersebut akan di dienkripsikan atau didecode menggunakan function `enkripsi2(temp)` untuk encode sedangkan untuk decode digunakkan fungsi `decode2(temp)`.

### Soal 2 - D + E
  Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori **“/home/[user]/hayolongapain_[kelompok].log”**. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem. Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level **INFO** dan **WARNING**. Untuk log level WARNING, digunakan untuk mencatat syscall *rmdir* dan *unlink*. Sisanya, akan dicatat pada level **INFO** dengan format sebagai berikut : 

  [Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]<br>
  Keterangan:<br>
  Level : Level logging&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dd : Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mm : Bulan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;yyyy : Tahun&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HH : Jam (dengan format 24 Jam)<br>
  MM : Menit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SS : Detik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CMD : System call yang terpanggil&nbsp;&nbsp;&nbsp;DESC : Informasi dan parameter tambahan <br>

```
void logged2(char *command, char *old, char *new)
{
  time_t times;
  struct tm *timeinfo;
  time(&times);
  timeinfo = localtime(&times);

  FILE *fptr;
  fptr = fopen("/home/ilham/hayolongapain_ita08.log", "a");
  if (fptr == NULL)
  {
    printf("[Error] : [Gagal dalam membuka file]");
    exit(1);
  }
  else
  {
    if (strcmp(command, "RENAME") == 0)
      fprintf(fptr, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900,
              timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
    else if (strcmp(command, "RMDIR") == 0 || strcmp(command, "UNLINK") == 0)
      fprintf(fptr, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900,
              timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
    else
      fprintf(fptr, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900,
              timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
  }
  fclose(fptr);
}
```
Dari source code diatas yang pertama kita lakukan adalah membuka file log tersebut yaitu "/home/csn/hayolongapain_ita08.log" dengan mode "a" yaitu *append*. Lalu dicek apakah file berhasil dibuka atau tidak, jika tidak maka outputkan errorrnya dan melakukan `exit(1)`. Jika berhasil maka printf kan ke dalam file tersebut sesuai dengan format yang ada dalam soal ke-2. Tutup file jika sudah tidak digunakkan lagi. Pencatatan log akan dilakukan pada setiap fungsi `xmp` yang ada seperti pada `xmp_read`, `xmp_readdir`, `xmp_mkdir`, dan lainnya.

### Hasil
![hasil1](image/sisop1.JPG)
![hasil1](image/sisop2.JPG)

### Kendala
- Sulit dalam memahami materi karena contoh yang sedikit.
- Kesulitan dalam manipulasi string.


